import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'variableAll.dart' as variableClass;
import 'style.dart' as styleUtamaClass;
import 'register.dart' as registerClass;
import 'webView.dart';


class FormAddScreen extends StatefulWidget {
  @override
  _FormAddScreenState createState() => _FormAddScreenState();
}

class _FormAddScreenState extends State<FormAddScreen> {

  void initState() {
    super.initState();

  }


  final GlobalKey<ScaffoldState> _scaffoldStateLogin = GlobalKey<ScaffoldState>();
  bool _isLoading = false;

  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  Future hilangkanSession() async {
    final pref = await SharedPreferences.getInstance();
    pref.setString('token', null);
    pref.setString('email', null);
    pref.setInt('id_user', null);
  }


  Future _Login() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token_fcm = prefs.getString("token_fcm");


    Dio dio = new Dio();
    final JsonDecoder _decoder = new JsonDecoder();

    Response<String> response =
    await dio.post(variableClass.URL_API + "login", data: {
      "email": _controllerEmail.text,
      "password": _controllerPassword.text,
      "token_fcm": token_fcm,
    });

    print(response.statusCode);

    var dataReturn = _decoder.convert(response.data);

    if (response.statusCode == 200) {
      if (dataReturn['status'] == 'success') {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        setState(() {
          prefs.setString('token', dataReturn["token"]);
          prefs.setString('user_token_webview', dataReturn["user_token_webview"]);
          prefs.setString('email', dataReturn["email"]);
          prefs.setString('nama', dataReturn["nama"]);
          prefs.setInt('id_user', dataReturn["id_user"]);

         print("iniToken:"+dataReturn["user_token_webview"]);

          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new webView(variableClass.URL_WEB+ "fromapps?user_token=" +dataReturn["user_token_webview"] +"&url=nasabah/proyek-donasi?from=apps_donasi","Daftar Donasi")
            ),
          );

        });
      } else {
        tampilSnackBar(dataReturn['message'], Colors.redAccent);
        setState(() => _isLoading = false);
        setState(() => _isLoading = false);

        //SharedPreferences prefsss = await SharedPreferences.getInstance();
        //print(prefsss.getString('page_welcome'));

        return;
      }
    } else {
      tampilSnackBar(variableClass.pesanKesalahanAPI, Colors.redAccent);
      setState(() => _isLoading = false);
      return;
    }

  }

  Future tampilSnackBar(tampilPesan, warnaBg) async {
    _scaffoldStateLogin.currentState.showSnackBar(
      SnackBar(
        content: Text(tampilPesan,
            style:
            styleUtamaClass.buildText(13, Colors.white, FontWeight.w100)),
        backgroundColor: warnaBg,
      ),
    );
    return;
  }


  DateTime currentBackPressTime;

  Future<bool> _onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Tekan Sekali lagi untuk keluar");
      return Future.value(false);
    }
    return Future.value(true);
  }




  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldStateLogin,
        body: _isLoading
            ? Center(
          child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(
                  styleUtamaClass.colorPrimaryDark)),
        )
            :






        CustomScrollView(

            slivers: <Widget>[

              SliverToBoxAdapter(
                child:

        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment:  MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15.0),
              child:
              Column(

                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.25,

                  ),
                  Image.asset('assets/images/logo.png',
                      width: MediaQuery.of(context).size.width * 0.8),
                  SizedBox(
                    height: 30.0,
                  ),
                  TextField(
                    controller: _controllerEmail,
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(
                        color: styleUtamaClass.colorPrimaryDark),
                    obscureText: false,
                    decoration: styleUtamaClass.buildInputPrimary(
                        'eMail',
                        Icon(Icons.email,
                            color: styleUtamaClass.colorPrimary)),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  TextField(
                    controller: _controllerPassword,
                    obscureText: true,
                    style: TextStyle(
                        color: styleUtamaClass.colorPrimaryDark),
                    decoration: styleUtamaClass.buildInputPrimary(
                        'Password',
                        Icon(Icons.lock,
                            color: styleUtamaClass.colorPrimary)),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  new InkWell(
                    onTap: () {
                      if (_controllerEmail.text == '') {
                        _scaffoldStateLogin.currentState.showSnackBar(
                          SnackBar(
                            content: Text("Silahkan isikan eMail",
                                style: styleUtamaClass.buildText(
                                    15, Colors.white, FontWeight.w100)),
                            backgroundColor: Colors.redAccent,
                          ),
                        );
                        return;
                      }
                      if (_controllerPassword.text == '') {
                        _scaffoldStateLogin.currentState.showSnackBar(
                          SnackBar(
                            content: Text("Silahkan isikan Password",
                                style: styleUtamaClass.buildText(
                                    15, Colors.white, FontWeight.w100)),
                            backgroundColor: Colors.redAccent,
                          ),
                        );
                        return;
                      }

                      setState(() => _isLoading = true);
                      _Login();
                    },
                    child: styleUtamaClass.buildButtonPrimary(
                        'Login', 60.0),
                  ),



                ],
              ),
            ),
          ],
        ),
        ),


              SliverToBoxAdapter(
                child: Container(
                    margin: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[

                        new InkWell(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                      registerClass.FormAddScreen()
                              ),
                            );
                          },
                          child:



                          RichText(
                            text: new TextSpan(
                              // Note: Styles for TextSpans must be explicitly defined.
                              // Child text spans will inherit styles from parent
                              style: new TextStyle(
                                color: styleUtamaClass.colorPrimaryDark,
                                fontSize: 15,
                                fontFamily: styleUtamaClass.fontFamilyDefalut,
                                fontWeight:  FontWeight.w100,
                              ),
                              children: <TextSpan>[
                                new TextSpan(text: 'kembali ke Halaman Registrasi'),

                              ],
                            ),

                          ),
                        ),


                      ],
                    )
                ),
              ),
      ]
        ),
      ),

    );
  }
}
