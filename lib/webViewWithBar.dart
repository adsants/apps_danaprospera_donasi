import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'style.dart' as styleUtamaClass;
import 'variableAll.dart' as variableClass;

/**
    class webView extends StatefulWidget {
    final url; //if you have multiple values add here
    final judul; //if you have multiple values add here
    webView(this.url, this.judul  {Key key})
    : super(key: key); //add also..example this.abc,this...



    @override
    State<StatefulWidget> createState() => webViewAppState();
    }
 **/

class webView extends StatefulWidget {
  final url; //if you have multiple values add here
  final judul; //if you have multiple values add here
  webView(this.url,this.judul,  {Key key})
      : super(key: key); //add also..example this.abc,this...

  @override
  State<StatefulWidget> createState() => webViewAppState();
}



class webViewAppState extends State<webView> {

  String urlBaru = "";
  final flutterWebviewPlugin = new FlutterWebviewPlugin();


  @override
  void initState() {

    flutterWebviewPlugin.close();
    urlBaru = widget.url;
    print("iniUrl : "+urlBaru);
  }

  void dispose() {
    // TODO: implement dispose
    flutterWebviewPlugin.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(

          backgroundColor: Colors.white10,
          elevation: 0,
        ),
        body:WebviewScaffold(
            url: urlBaru,
            withJavascript: true, // run javascript
            withZoom: false, // if you want the user zoom-in and zoom-out
            hidden: true , // put it true if you want to show CircularProgressIndicator while waiting for the page to load

            appBar: AppBar(
              backgroundColor: styleUtamaClass.colorPrimaryDark,
              title: Text(
                widget.judul,
                style: styleUtamaClass.buildText(
                    20, styleUtamaClass.colorPutih, FontWeight.normal),
              ),
            ),


            initialChild: Container( // but if you want to add your own waiting widget just add InitialChild
              color: Colors.white,
              child:
              Center(
                child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        styleUtamaClass.colorPrimaryDark)),
              )
              ,)



        )
    );


  }
}
