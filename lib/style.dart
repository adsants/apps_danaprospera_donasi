import 'package:flutter/material.dart';

const colorPrimary      = Color(0xffe50189);
const colorPrimaryDark  = Color(0xFF910B76);
const colorPrimaryUngu  = Color(0xFF8D6DC9);
const colorPutih        = Colors.white;
const colorHitam        = Colors.black;
const colorAbu        = Color(0xFFf3f3f3);
const colorAbuGelap        = Color(0xFF57606f);

const fontFamilyDefalut  =  'GoogleSans';

buildText(double ukuranFont,  colorName , tebalFont) {
  return
    TextStyle(
      color: colorName,
      fontSize: ukuranFont,
      fontFamily: fontFamilyDefalut,
      fontWeight: tebalFont,
    );
}


buildInputPrimary(labelTextName, Icon iconName){
  return InputDecoration(
    contentPadding: const EdgeInsets.all(0.0),

    labelText   : labelTextName,
    labelStyle  : buildText(15, colorPrimaryDark, FontWeight.normal),
    prefixIcon  : Padding(
      padding   : EdgeInsets.only(right: 0),
      child     : iconName,
    ),
    border: OutlineInputBorder(
      borderSide  :  BorderSide(color: colorPrimaryUngu, width: 0.0),
      borderRadius: BorderRadius.circular(0.0),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: colorPrimaryUngu, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: colorPrimaryUngu, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    errorBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: colorPrimaryUngu, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: colorPrimary, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    disabledBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: colorPrimary, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
  );
}

buildInputDisabled(labelTextName, Icon iconName){
  return InputDecoration(

    labelText   : labelTextName,
    labelStyle  : buildText(15, colorPrimaryDark, FontWeight.normal),
    prefixIcon  : Padding(
      padding   : EdgeInsets.only(right: 0),
      child     : iconName,
    ),
    border: OutlineInputBorder(
      borderSide  :  BorderSide(color: Colors.black26, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: Colors.black26, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: Colors.black26, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    errorBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: Colors.black26, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: Colors.black26, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
    disabledBorder: OutlineInputBorder(
      borderSide:  BorderSide(color: Colors.black26, width: 0.0),
      borderRadius: BorderRadius.circular(10.0),
    ),
  );
}

buildButtonPrimary(textButton,double heightVal){
  return new Padding(
    padding :EdgeInsets.only(top:10.0),
    child:Container(
      height: heightVal,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              colorPrimaryDark,
              colorPrimary,
            ],
            begin: Alignment.centerRight,
            end: Alignment.centerLeft
        ),
        borderRadius: new BorderRadius.circular(10.0),
      ),
      child: new Center(
        child: new Text(
          textButton,
          style: buildText(20, colorPutih, FontWeight.normal),
        ),
      ),
    ),
  );
}




buildButtonRegister(textButton,double heightVal){
  return new Padding(
    padding :EdgeInsets.only(top:10.0),
    child:Container(
      height: heightVal,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            colors: [
              Colors.lightGreen,
              Colors.lightGreenAccent,
            ],
            begin: Alignment.centerRight,
            end: Alignment.centerLeft
        ),
        borderRadius: new BorderRadius.circular(10.0),
      ),
      child: new Center(
        child: new Text(
          textButton,
          style: buildText(20, colorPrimaryDark, FontWeight.normal),
        ),
      ),
    ),
  );
}




buildButton(textButton,double heightVal,double fontSize, colorName, colorText){
  return new Padding(
    padding :EdgeInsets.only(top:10.0),
    child:Container(
      height: heightVal,
      decoration: BoxDecoration(
        color: colorName,
        borderRadius: new BorderRadius.circular(10.0),
      ),
      child: new Center(
        child: new Text(
          textButton,
          style: buildText(fontSize, colorText, FontWeight.normal),
        ),
      ),
    ),
  );
}

