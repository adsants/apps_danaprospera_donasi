import 'package:flutter/services.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';


import 'variableAll.dart' as variableClass;
import 'splashScreen.dart' as splashScreenClass;
import 'webView.dart';

void main() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
    runApp(


      MaterialApp(

        debugShowCheckedModeBanner: false,
        home: PushMessagingExample(),
      ),
    );

}

/*
This is the main class that contains FCM init stuff needed and the how the
UI display looks like.
 */
class PushMessagingExample extends StatefulWidget {


  @override
  _PushMessagingExampleState createState() => _PushMessagingExampleState();
}

class _PushMessagingExampleState extends State<PushMessagingExample> {


  String _homeScreenText = "Waiting for token...";

  bool _newNotification = false;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        setState(() {
          _newNotification = true;
        });
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        //print(message['data']['status']);
        _navigateToItemDetail(message);
      },
    );

    //Needed by iOS only
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    //Getting the token from FCM
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "Push Messaging token: \n\n $token";
        createTokenFCM(token);
      });
      print(_homeScreenText);
    });
  }

  Future createTokenFCM(token) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('token_fcm', token);
      prefs.setString('versi', variableClass.versi);
    });

  }

  @override


  Widget build(BuildContext context) {
    return new

    MaterialApp(
      title: 'Danaprospera Peduli',
      debugShowCheckedModeBanner: false,
      home: splashScreenClass.splashScreenApp(),
    );
  }

  //PRIVATE METHOD TO HANDLE NAVIGATION TO SPECIFIC PAGE
  void _navigateToItemDetail(Map<String, dynamic> message) {
    final MessageBean item = _itemForMessage(message);
    // Clear away dialogs
    Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

}

final Map<String, MessageBean> _items = <String, MessageBean>{};
MessageBean _itemForMessage(Map<String, dynamic> message) {
  //If the message['data'] is non-null, we will return its value, else return map message object
  final dynamic data = message['data'] ?? message;
  final String judul       = data['judul'];
  final String url         = data['url'];
  final String referralCode = data['referral_code'];
  final MessageBean item    = _items.putIfAbsent(
      url, () => MessageBean(url: url,judul: judul,referralCode: referralCode))
    ..status = data['status'];
  return item;
}

//Model class to represent the message return by FCM
class MessageBean {
  MessageBean({this.judul,this.url,this.referralCode});
  final String judul;
  final String url;
  final String referralCode;

  StreamController<MessageBean> _controller =
  StreamController<MessageBean>.broadcast();
  Stream<MessageBean> get onChanged => _controller.stream;

  String _status;
  String get status => _status;
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

  static final Map<String, Route<void>> routes = <String, Route<void>>{};
  Route<void> get route {
    final String routeName = 'iniRoute';
    print(url);
    return routes.putIfAbsent(
      routeName,
          () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => new webView(url,judul),
      ),
    );


  }
}