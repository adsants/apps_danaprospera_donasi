import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'style.dart' as styleUtamaClass;
import 'login.dart' as loginClass;
import 'variableAll.dart' as variableClass;

class webView extends StatefulWidget {
  final url; //if you have multiple values add here
  final judul; //if you have multiple values add here
  webView(this.url,this.judul,  {Key key})
      : super(key: key); //add also..example this.abc,this...

  @override
  State<StatefulWidget> createState() => webViewAppState();
}



class webViewAppState extends State<webView> {

  String urlBaru = "";
  final flutterWebviewPlugin = new FlutterWebviewPlugin();


  @override
  void initState() {

    flutterWebviewPlugin.close();
    urlBaru = widget.url;
    print("iniUrl : "+urlBaru);
  }

  void dispose() {
    // TODO: implement dispose
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  Future hilangkanSession() async {
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
  }


  DateTime currentBackPressTime;

  Future<bool> _onWillPop()  async{
    return true;
  }

  @override
  Widget build(BuildContext context) {

    return
      new
        Scaffold(
          appBar: AppBar(

            elevation: 0.9,
            centerTitle: true,
            iconTheme: new IconThemeData(color: Colors.white),

            title: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new Image.asset(
                  'assets/images/logo_putih.png',
                  height: 22.0,
                ),
              ],
            ),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: <Color>[
                    styleUtamaClass.colorPrimaryDark,
                    styleUtamaClass.colorPrimary
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              // Using Stack to show Notification Badge
              new Stack(
                children: <Widget>[

                  new IconButton(
                      icon: Icon(
                        Icons.exit_to_app,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        hilangkanSession();

                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new loginClass.FormAddScreen()),
                        );
                        //MaterialPageRoute(builder: (context) => new messageClass.MyApp());
                      }),
                ],
              ),
            ],
          ),

          body: WillPopScope( onWillPop: _onWillPop,  child:
            WebviewScaffold(
              url: urlBaru,
              withJavascript: true, // run javascript
              withZoom: false, // if you want the user zoom-in and zoom-out
              hidden: true , // put it true if you want to show CircularProgressIndicator while waiting for the page to load
            )
          )

        );



  }
}
