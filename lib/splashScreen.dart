import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'dart:async';

import 'package:DanaprosperaPeduli/login.dart' as loginClass;
import 'package:DanaprosperaPeduli/register.dart' as registerClass;
import 'package:DanaprosperaPeduli/webView.dart';
import 'package:DanaprosperaPeduli/variableAll.dart' as variableClass;



class splashScreenApp extends StatefulWidget {
  @override
  _splashScreenApp createState() => _splashScreenApp();
}

class _splashScreenApp extends State<splashScreenApp> {


  @override
  void initState() {
    super.initState();
    loadData();
  }


  var cekWelcome = "Belum";

  bool _isLoggedIn    = false;


  Future<Timer> loadData() async {

    String udid = await FlutterUdid.udid;
    print(udid);

    return new Timer(Duration(seconds: 2), onDoneLoading);
  }

  onDoneLoading() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if ( prefs.getString("email") != null ){
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
            new webView(variableClass.URL_WEB+ "fromapps?user_token=" + prefs.getString("user_token_webview")  +"&url=nasabah/proyek-donasi?from=apps_donasi","Daftar Donasi")
        ),
      );
    }else{
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          //builder: (context) => new registerClass.FormAddScreen()),
            builder: (context) => new registerClass.FormAddScreen()),
      );
    }

  }


  @override
  Widget build(BuildContext context) {
    return
      Container(
          color: Colors.white,
          child:Center(
              child: Image.asset(
                  'assets/images/logo_splash_screen.png',
                  height: MediaQuery.of(context).size.width * 0.5,
                  width: MediaQuery.of(context).size.width * 0.5,
              )
          )
      );
  }





}